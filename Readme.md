Fast copy/move of chosen file type to folder of choose 
========= 

Usage:
=========  
  
Have many folder/subfolder or a drive with pictures/movies.   
Want to collect all picures/movies to a singel folder.  
Be careful to use `move` on a whole drive,  
A lot picture that belong to application/games can be moved.
---
How to use: 
=========  
Run command: `python find_collect_files.py`
```sh
search_folders = (r'E:\test', r'E:\test1',)
file_extension = ('.jpg', '.png', '.gif')
option = copy #move
destination_folder = r'E:\all_pic'
copy_move_files(search_folders, destination_folder, file_extension, error, option)
```
 Copy all jpg,png,gif in folders test and test1 to all_pic folder.  
 Option `copy` will as name say copy files to all_pic folder,orginal files stay put.  
 `move` will move files from orginal folders to all_pic folder.  
 Error on copy/move of files will be saved to error.log.
 ---
Speed?
=========
Code use [scandir](https://github.com/benhoyt/scandir) by Ben Hoyt.  
That speed up the recursive search alot over `os.walk()`  
About 8-9 times as fast on Windows, and about 2-3 times as faster on Linux and Mac OS X.  
Testet with [PyPy](http://pypy.org/) that also take time furder down.














