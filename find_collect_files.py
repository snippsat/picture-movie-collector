import os, sys
from shutil import copy, move
from itertools import chain
from os.path import splitext
import time
import scandir # *
''' * scandir
Copyright (c) 2012, Ben Hoyt
All rights reserved.
'''

def timeit(f):
    '''Timing how long time move/copy files takes'''
    def timed(*args):
        ts = time.time()
        result = f(*args)
        te = time.time()
        print 'func:{!r}  {!r} took: {:.2f} sec'.format\
        (f.__name__, args, te-ts)
        return result
    return timed

def error(arg=''):
    '''Save all error in a log'''
    with open('Error.log', 'w') as sys.stderr:
        sys.stderr.write('{}\n'.format(str(arg)))

@timeit
def copy_move_files(search_folders, destination_folder, file_extension, error, option):
    '''Function for copy/move files'''
    counter = 0
    for path, dirs, files in chain.from_iterable(scandir.walk(path) for path in search_folders):
        for f_name in files:
            file_name = os.path.join(path, f_name)
            temp = splitext(file_name)
            file_name = temp[0] + temp[1].lower()
            if file_name.endswith(file_extension):
                counter += 1
            try:
                option(file_name, destination_folder)
                #print '{} {}'.format(file_name, counter) #see what happends
            except Exception as err:
                error(err)         

if __name__ == '__main__':
    '''
    Example of usage:
    ---
   search_folders = (r'E:\test', r'E:\test1',)
   file_extension = ('.jpg', '.png', '.gif')
   option = copy #move
   destination_folder = r'E:\all_pic'
   copy_move_files(search_folders, destination_folder, file_extension, error, option)
    ---
    Copy all jpg,png,gif in folders test and test1 to all_pic folder.
    Option copy will as name say copy files,orginal files stay put.
    '''
    search_folders = (r'E:\test',)
    file_extension = ('.jpg', '.png', '.gif')
    option = copy # move
    destination_folder = r'E:\all_pic'
    copy_move_files(search_folders, destination_folder, file_extension, error, option)







